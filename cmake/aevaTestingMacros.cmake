# aeva_plugin_init_file - Create a plugin initialization file.
#
# Usage:
#   aeva_plugin_init_file(plugin_paths plugin_list)
# where
#   plugin_paths  - is the name of a variable containing paths that may hold plugins
#   plugin_list   - is the name of a variable holding a list of plugins
#
# This macro creates a ".plugin" file in ${CMAKE_RUNTIME_OUTPUT_DIRECTORY} (usu. "bin")
# that lists plugins to load for testing as well as general use.
#
# Run aeva with the PV_PLUGIN_DEBUG environment variable set to
# see whether this file is being used and, if so, what plugins are loaded.
function(aeva_plugin_init_file plugin_paths plugin_list)
  set(aeva_plugin_xml "<?xml version=\"1.0\"?>\n<Plugins>\n")
  foreach (aeva-plugin-name IN LISTS ${plugin_list})
    set(found FALSE)
    foreach (plugin-path IN LISTS ${plugin_paths})
      if (WIN32 AND NOT CYGWIN)
        set(full-plugin-path "${plugin-path}/${aeva-plugin-name}.dll")
      elseif (APPLE)
        set(full-plugin-path "${plugin-path}/lib${aeva-plugin-name}.dylib")
      elseif (UNIX)
        set(full-plugin-path "${plugin-path}/lib${aeva-plugin-name}.so")
      endif()

      if (EXISTS ${full-plugin-path})
        set(aeva_plugin_xml
          "${aeva_plugin_xml}  <Plugin name=\"${aeva-plugin-name}\" auto_load=\"1\" filename=\"${full-plugin-path}\" />\n")
        set(found TRUE)
        break()
      endif()
    endforeach()
    if (NOT found)
      # Assume ParaView will know how to find it.
      set(aeva_plugin_xml "${aeva_plugin_xml}  <Plugin name=\"${aeva-plugin-name}\" auto_load=\"1\" />\n")
    endif ()
  endforeach()

  set(aeva_plugin_xml "${aeva_plugin_xml}</Plugins>\n")
  file(WRITE "${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/.plugins" "${aeva_plugin_xml}")

  # Install the .plugins configuration file.
  if(APPLE)
    set(install_plugin_path "${VTK_INSTALL_RUNTIME_DIR}/aeva.app/Contents/Plugins")
  else()
    set(install_plugin_path "${VTK_INSTALL_RUNTIME_DIR}")
  endif()

  install(
    FILES "${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/.plugins"
    DESTINATION "${install_plugin_path}"
    COMPONENT Runtime
  )
endfunction()
