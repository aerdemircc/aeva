## Test Infrastructure Changes

+ ModelBuilder now names the default popup window so that tests
  which rely on the context menu showing toolbars and dock widgets
  will play back properly. Pre-existing tests that relied on
  the old name would not consistently play, but if any exist they
  need to be re-recorded or edited to reflect the new name.
+ SMTK now names many Qt widgets so that test failures due to name
  mismatches should not happen.
+ aeva now has XML GUI tests of some basic functionality.
